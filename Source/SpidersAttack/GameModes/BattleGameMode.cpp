#include "BattleGameMode.h"
#include "PlayerControllers/SpidersAttackPlayerController.h"
#include "Pawns/Characters/SpidersAttackCharacter.h"
#include "GameStates/BattleGameState.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"

ABattleGameMode::ABattleGameMode()
{
	// Set main classes of the level
	GameStateClass = ABattleGameState::StaticClass();
	PlayerControllerClass = ASpidersAttackPlayerController::StaticClass();

	// Set default character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Pawns/Characters/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ABattleGameMode::BeginPlay()
{
	SpawnEnemies();
}

void ABattleGameMode::SpawnEnemies()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), EnemyTargetPoints);
}
