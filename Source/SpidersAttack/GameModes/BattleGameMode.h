#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BattleGameMode.generated.h"

UCLASS(minimalapi)
class ABattleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABattleGameMode();

protected:
	void BeginPlay() override;

	void SpawnEnemies();

	TArray<class AActor*> EnemyTargetPoints;
};
