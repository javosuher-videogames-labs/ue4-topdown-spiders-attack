#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

// Delegate to execute when the character die
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam
(
	FCharacterDie,
	class ABaseCharacter*, CharacterDead
);

UCLASS()
class SPIDERSATTACK_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter")
		FORCEINLINE int GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter")
		void SetHealth(int CharacterHealth);

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter")
		void IncrementHealth(int HealthToIncrement);

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter")
		void DecrementHealth(int HealthToDecrement);

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter")
		FORCEINLINE bool IsCharacterDead() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter")
		int Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter")
		int MaximumHealth;

	UPROPERTY(BlueprintAssignable, Category = "BaseCharacter")
		FCharacterDie CharacterDie;

protected:

	void CheckIfIsDead();

	bool IsDead;
};
