#include "BaseCharacter.h"

ABaseCharacter::ABaseCharacter() : Super()
{
}

int ABaseCharacter::GetHealth() const
{
	return Health;
}

void ABaseCharacter::SetHealth(int CharacterHealth)
{
	Health = CharacterHealth;
}

void ABaseCharacter::IncrementHealth(int HealthToIncrement)
{
	Health = FMath::Clamp(Health + HealthToIncrement, 0, MaximumHealth);
}

void ABaseCharacter::DecrementHealth(int HealthToDecrement)
{
	Health = FMath::Clamp(Health - HealthToDecrement, 0, MaximumHealth);
	CheckIfIsDead();
}

bool ABaseCharacter::IsCharacterDead() const
{
	return IsDead;
}

void ABaseCharacter::CheckIfIsDead()
{
	if(Health == 0)
	{
		IsDead = true;
		CharacterDie.Broadcast(this);
	}
}

