// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawns/Characters/BaseCharacter.h"
#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class SPIDERSATTACK_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
};
