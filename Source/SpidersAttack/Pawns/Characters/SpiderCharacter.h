#pragma once

#include "CoreMinimal.h"
#include "Pawns/Characters/BaseCharacter.h"
#include "SpiderCharacter.generated.h"

/**
 * 
 */
UCLASS()
class SPIDERSATTACK_API ASpiderCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
public:
	ASpiderCharacter();
};
