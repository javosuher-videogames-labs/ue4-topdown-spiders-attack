#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * Player controller of the main screen of the game 
 */
UCLASS()
class SPIDERSATTACK_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMainPlayerController();
	void ConfigureViewport();

private:
	void BeginPlay() override;
	void SetupInputComponent() override;

	void TeleportToBattle();

	FName LevelToTravel;

	// Main menu variables
	TSubclassOf<class UUserWidget> MainMenuClass;
	TWeakObjectPtr<class UUserWidget> MainMenu;
};
