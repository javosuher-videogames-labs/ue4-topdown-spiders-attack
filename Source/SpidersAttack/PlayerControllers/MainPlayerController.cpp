#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AMainPlayerController::AMainPlayerController()
{
	// Get main menu
	int x = 0;
	auto MenuAssetClass = ConstructorHelpers::FClassFinder<UUserWidget>(TEXT("/Game/UI/W_MainMenu"));
	if(MenuAssetClass.Succeeded())
	{
		MainMenuClass = MenuAssetClass.Class;
	}

	// Class parameters values
	LevelToTravel = "BattleMap";
}

void AMainPlayerController::ConfigureViewport()
{
	// Create menu and set to viewport
	MainMenu = CreateWidget<UUserWidget>(this, MainMenuClass);
	MainMenu->AddToViewport();
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();
	ConfigureViewport();
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Binding any key
	InputComponent->BindKey
	(
		EKeys::AnyKey, 
		IE_Pressed, 
		this, 
		&AMainPlayerController::TeleportToBattle
	);
}

void AMainPlayerController::TeleportToBattle()
{
	UGameplayStatics::OpenLevel(GetWorld(), LevelToTravel);
}
