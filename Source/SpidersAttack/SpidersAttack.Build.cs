using UnrealBuildTool;

public class SpidersAttack : ModuleRules
{
    public SpidersAttack(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core", 
                "CoreUObject", 
                "Engine", 
                "InputCore", 
                "HeadMountedDisplay", 
                "NavigationSystem", 
                "AIModule", 
                "UMG",
                "Slate",
                "SlateCore"
            });
    }
}
